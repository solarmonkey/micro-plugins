package requestid

import (
	"context"

	"github.com/google/uuid"
	"github.com/micro/go-micro/v2/client"
	meta "github.com/micro/go-micro/v2/metadata"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/server"
	"google.golang.org/grpc/metadata"
)

type requestID struct{}

//ServerWrapper server wrapper
func ServerWrapper() server.HandlerWrapper {
	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, resp interface{}) error {
			var rid string
			md, ok := metadata.FromIncomingContext(ctx)
			if !ok {
				rid = uuid.New().String()
			}
			rids := md.Get("X-Request-ID")
			if len(rids) > 0 {
				rid = rids[0]
			}
			ctx = context.WithValue(ctx, requestID{}, rid)
			return fn(ctx, req, resp)
		}
	}
}

//ClientWrapper client wrapper
func ClientWrapper() client.CallWrapper {
	return func(fn client.CallFunc) client.CallFunc {
		return func(ctx context.Context, node *registry.Node, req client.Request, resp interface{},
			opts client.CallOptions) error {
			t := ctx.Value(requestID{})
			rid, ok := t.(string)
			if !ok {
				rid = uuid.New().String()
				ctx = context.WithValue(ctx, requestID{}, rid)
			} else {
				if len(rid) == 0 {
					rid = uuid.New().String()
					ctx = context.WithValue(ctx, requestID{}, rid)
				}
			}
			md, ok := meta.FromContext(ctx)
			if !ok {
				md = meta.Metadata{}
			}
			md.Set("X-Request-ID", rid)
			ctx = meta.NewContext(ctx, md)
			return fn(ctx, node, req, resp, opts)
		}
	}
}

//GetXRequestID get x-request-id
func GetXRequestID(ctx context.Context) string {
	t := ctx.Value(requestID{})
	rid, ok := t.(string)
	if !ok {
		rid = uuid.New().String()
		ctx = context.WithValue(ctx, requestID{}, rid)
	} else {
		if len(rid) == 0 {
			rid = uuid.New().String()
			ctx = context.WithValue(ctx, requestID{}, rid)
		}
	}
	return rid
}
