package main

import (
	"context"
	"fmt"
	"os"

	"github.com/micro/cli/v2"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/client"
	"github.com/micro/go-micro/v2/registry"

	"bitbucket.org/solarmonkey/micro-plugins/pkg/v2/requestid"
	"bitbucket.org/solarmonkey/micro-plugins/test/v2/requestid/greeter"
)

//GreeterHandler service
type GreeterHandler struct {
}

//Hello implemention
func (rh *GreeterHandler) Hello(ctx context.Context, req *greeter.HelloRequest,
	resp *greeter.HelloResponse) error {
	fmt.Println(requestid.GetXRequestID(ctx))
	fmt.Printf("hello from %s\n", req.Name)
	resp.Reply = "hello, " + req.Name
	return nil
}

//Goodbye implemention
func (rh *GreeterHandler) Goodbye(ctx context.Context, req *greeter.GoodbyeRequest,
	resp *greeter.GoodbyeResponse) error {
	fmt.Println(requestid.GetXRequestID(ctx))
	fmt.Printf("goodbye from %s\n", req.Name)
	resp.Reply = "goodbye, " + req.Name
	return nil
}

func runClient(ms micro.Service) {
	cli := greeter.NewGreeterService("ms", ms.Client())
	resp, err := cli.Hello(context.Background(), &greeter.HelloRequest{Name: "solarmonkey"})
	if err != nil {
		fmt.Println(err)
	}

	if resp != nil {
		fmt.Println(resp)
	}
}

func printClientRequestID() client.CallWrapper {
	return func(fn client.CallFunc) client.CallFunc {
		return func(ctx context.Context, node *registry.Node, req client.Request,
			resp interface{}, opts client.CallOptions) error {
			err := fn(ctx, node, req, resp, opts)
			fmt.Println(requestid.GetXRequestID(ctx))
			return err
		}
	}
}

func main() {
	ms := micro.NewService(
		micro.Name("ms"),
		micro.Address("0.0.0.0:19001"),
		micro.WrapHandler(requestid.ServerWrapper()),
		micro.WrapCall(requestid.ClientWrapper()),
		micro.WrapCall(printClientRequestID()),
		micro.Flags(&cli.BoolFlag{
			Name:  "c",
			Usage: "Launch the client",
		}),
	)
	ms.Init(
		micro.Action(func(c *cli.Context) error {
			if c.Bool("c") {
				runClient(ms)
				os.Exit(0)
			}
			return nil
		}),
	)

	greeter.RegisterGreeterHandler(ms.Server(), &GreeterHandler{})

	err := ms.Run()
	if err != nil {
		fmt.Println(err)
	}
}
